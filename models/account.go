package models

import "time"

type Account struct {
	ID          uint      `gorm:"primaryKey"`
	CreatedAt   time.Time `gorm:"created_at"`
	AccountName string    `gorm:"account_name"`
	Balance     int       `gorm:"balance"`
	Active      bool      `gorm:"active"`
}
