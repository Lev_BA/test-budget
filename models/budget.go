package models

import "time"

type Budget struct {
	ID              uint      `gorm:"primaryKey"`
	CreatedAt       time.Time `gorm:"created_at"`
	Account         string    `gorm:"account"`
	TypeTransaction string    `gorm:"type_transaction"`
	ToTransaction   string    `gorm:"to_transaction"`
	Amount          int       `gorm:"amount"`
}
