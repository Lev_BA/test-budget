package account

type Account struct {
}

type AccountController interface {
	Expend()
	Income()
	Transfer()
}

func (a *Account) Expend() {

}

func (a *Account) Income() {

}

func (a *Account) Transfer() {

}
