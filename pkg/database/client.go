package database

import (
	"errors"
	"fmt"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

type Database struct {
	user         string
	password     string
	host         string
	port         string
	databaseName string
}

func NewDatabase(user, password, host, port, databaseName string) *Database {
	return &Database{
		user:         user,
		password:     password,
		host:         host,
		port:         port,
		databaseName: databaseName,
	}
}

func (db *Database) Connect() (*gorm.DB, error) {
	dsn := fmt.Sprintf("host=%s user=%s password=%s dbname=%s port=%s sslmode=disable TimeZone=Europe/Moscow",
		db.host,
		db.user,
		db.password,
		db.databaseName,
		db.port,
	)

	newDB, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})
	if err != nil {
		return nil, errors.New("error connect database")
	}

	return newDB, nil
}
