package main

import (
	"context"
	"github.com/gorilla/mux"
	"html/template"
	"log"
	"net/http"
	"os"
	"os/signal"
	"test-budget/pkg/database"
	"test-budget/pkg/routes"
	"time"
)

// Задание: сервис для ведения личного бюджета
// Должен представлять из себя веб-приложение, в котором пользователь заносит информацию по движение своих денежных средств. У пользователя может бы несколько счетов и три типа транзакций: расход, доход, перевод между счетами
// - Реализовать работу с базой данных
// - Реализовать REST API для работы с сервисом
// - (Опционально) Написать документацию к созданному REST API в формате OpenAPI

func main() {
	var newDB = database.NewDatabase("postgres", "1111", "localhost", "5432", "test-budget")

	_, err := newDB.Connect()
	if err != nil {
		log.Printf("connect database error: %s", err)
	}

	routers := mux.NewRouter()
	routers.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		tmpl, err := template.ParseFiles("template/index.tpl")
		if err != nil {
			log.Fatal(err)
		}
		tmpl.Execute(w, nil)
	})
	routers.HandleFunc("/api/v1/expend", routes.Expand).Methods("POST, OPTIONS")
	routers.HandleFunc("/api/v1/income", routes.Income).Methods("POST, OPTIONS")
	routers.HandleFunc("/api/v1/transfer", routes.Transfer).Methods("POST, OPTIONS")

	s := &http.Server{
		Addr:         ":8080",
		WriteTimeout: time.Minute * 5,
		ReadTimeout:  time.Minute * 5,
		IdleTimeout:  time.Second * 1,
		Handler:      routers,
	}

	go func() {
		s.ListenAndServe()
	}()

	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt)
	<-quit
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	s.Shutdown(ctx)
}
