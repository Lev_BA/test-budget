FROM golang:alpine

WORKDIR /app

COPY . .

RUN go build ./cmd/app/budget.go

ENTRYPOINT ["./budget"]