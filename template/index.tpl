<!doctype html>
<html lang="ru">
<head>
  <meta charset="UTF-8">
  <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">
</head>
<body>
  <div class="main " id="main">
    <div class="container">
      <div class="row">
        <div class="col-4">
          <form method="post" action="/api/v1/income">
            <div class="form-group">
              <label for="exampleIncome">Доход</label>
              <input type="text" class="form-income" id="exampleIncome" name="form-income">
              <small class="form-text text-muted">Введите ваш доход</small>
            </div>
            <div class="form-group form-check">
              <input type="checkbox" class="form-transfer" id="exampleCheckIncome1">
              <label class="form-check-label" for="exampleCheckIncome1">Счет 1</label>
              <input type="checkbox" class="form-transfer" id="exampleCheckIncome2">
              <label class="form-check-label" for="exampleCheckIncome2">Счет 2</label>
              <input type="checkbox" class="form-transfer" id="exampleCheckIncome3">
              <label class="form-check-label" for="exampleCheckIncome3">Счет 3</label>
            </div>
            <button type="submit" class="btn btn-primary">Отправить</button>
          </form>
        </div>
        <div class="col-4">
          <form method="post" action="/api/v1/expend">
            <div class="form-group">
              <label for="exampleExpend">Расход</label>
              <input type="text" class="form-expend" id="exampleExpend" name="form-expend">
              <small class="form-text text-muted">Введите ваш расход</small>
            </div>
            <div class="form-group form-check">
              <input type="checkbox" class="form-transfer" id="exampleCheckExpend1">
              <label class="form-check-label" for="exampleCheckExpend1">Счет 1</label>
              <input type="checkbox" class="form-transfer" id="exampleCheckExpend2">
              <label class="form-check-label" for="exampleCheckExpend2">Счет 2</label>
              <input type="checkbox" class="form-transfer" id="exampleCheckExpend3">
              <label class="form-check-label" for="exampleCheckExpend3">Счет 3</label>
            </div>
            <button type="submit" class="btn btn-primary">Отправить</button>
          </form>
        </div>
        <div class="col-4">
          <form method="post" action="/api/v1/transfer">
            <div class="form-group">
              <label for="exampleTransfer">Перевод</label>
              <input type="text" class="form-expend" id="exampleTransfer" name="form-transfer">
              <small class="form-text text-muted">Введите ваш перевод с счета на счет</small>
            </div>
            <div class="form-group form-check">
              <input type="checkbox" class="form-transfer" id="exampleCheckTransfer1">
              <label class="form-check-label" for="exampleCheckTransfer1">Счет 1</label>
              <input type="checkbox" class="form-transfer" id="exampleCheckTransfer2">
              <label class="form-check-label" for="exampleCheckTransfer2">Счет 2</label>
              <input type="checkbox" class="form-transfer" id="exampleCheckTransfer3">
              <label class="form-check-label" for="exampleCheckTransfer3">Счет 3</label>
            </div>
            <button type="submit" class="btn btn-primary">Отправить</button>
          </form>
        </div>
      </div>
    </div>
  </div>
  <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-fQybjgWLrvvRgtW6bFlB7jaZrFsaBXjsOMm/tB9LTS58ONXgqbR9W8oWht/amnpF" crossorigin="anonymous"></script>
</body>
</html>